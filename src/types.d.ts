export interface Category {
  id: number
  name: string
  created_at: string
  updated_at: string
}
export interface Person {
  id: number
  name: string
  birthdate: string
  created_at: string
  updated_at: string
}
export interface Movie{
  id: number
  title: string
  description: string
  duration: string
  year: string
  director_id: number
  created_at: string
  updated_at: string
  director: Person
  categories: Category[]
  actors: Person[]

}
