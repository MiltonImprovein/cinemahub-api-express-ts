import { Movie } from '../types'
import moviesData from './movies.json'

const movies: Movie[] = moviesData as Movie[]

export const getMovies = (): Movie[] => movies

export const addMovie = (): undefined => undefined
