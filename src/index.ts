import express from 'express'

import moviesRouter from './routes/movies'

const app = express()
app.use(express.json())

const PORT = 3000

app.get('/ping', (_, res) => {
  console.log('someone pinged here!')
  res.send('pong')
})

app.use('/api/movies', moviesRouter)

app.listen(PORT, () => {
  console.log(`Server running on post ${PORT}`)
})
