import express from 'express'
import * as moviesServices from '../services/moviesServices'

const router = express.Router()

router.get('/', (_req, res) => {
  res.send(moviesServices.getMovies())
})

router.post('/', (_req, res) => {
  res.send('adding new movie')
})
export default router
